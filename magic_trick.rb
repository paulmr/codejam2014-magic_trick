
class MagicTrick
  def initialize
    @numrows  = 4
    @rowcount = 0
    # create and array for two results, each containing an array of
    # rows
    @res = Array.new(2) { Array.new() }
    # store the two answers
    @ans = []
  end
  def rowCount
    @numrows
  end
  def addRow(str)
    # split on the default -- whitespace
    @res[@rowcount/@numrows].push(str.split)
    @rowcount += 1
  end
  def addAns(n)
    @ans.push(n)
  end
  def getRow(res, row)
    @res[res][row]
  end
  def findCard()
    row1 = getRow(0, @ans[0] - 1) # convert to zero-indexed
    row2 = getRow(1, @ans[1] - 1)
    found = row1 & row2
    if found.length == 1
      return found.pop
    elsif found.length < 1
      return "Volunteer cheated!"
    end
    # default
    return "Bad magician!"
  end
end

# pass it a file, and it will convert into a magic trick class and
# return the result
def readArrangement(trick, f)
  for n in 1..trick.rowCount do
    trick.addRow(f.gets)
  end
end  

def readTestCase(f)
  trick = MagicTrick.new
  trick.addAns(f.gets.to_i)
  readArrangement(trick, f)
  trick.addAns(f.gets.to_i)
  readArrangement(trick, f)
  trick
end

f = File.new(ARGV[0], "r")
# how many test cases are we reading from this file?
count = f.gets.to_i
for casenum in 1..count do
  puts "Case #" + casenum.to_s + ": " + readTestCase(f).findCard
end
